DESTDIR=$(HOME)/.local/bin
install:
	mkdir -p $(DESTDIR)
	cp scripts/* $(DESTDIR)
	make -C startup/
	make -C startup/ install
	make -C startup/ clean
