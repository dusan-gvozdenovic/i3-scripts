using Json;
using Startup.Utils;

namespace Startup {
	class Startup {
		static Json.Object root;

		public static int main (string[] args) {
			root = get_document ();
			int success = 0, fail = 0;
			foreach (unowned string program_name in root.get_members ()) {
				Program program = load_program (program_name);
				program.start ();
				if (program.status == StartupStatus.FAILURE) {
					stderr.printf ("%s\n", program.error_message);
					systemd_cat (program.error_message);
					fail++;
				} else if (program.status == StartupStatus.SUCCESS) {
					stdout.printf ("Successfuly started program: %s\n", program_name);
					success++;
				}
			}
			stdout.printf (@"Started: $success Failed: $fail\n");
			return 0;
		}

		public static Json.Object get_document () {
			try {
				var parser = new Json.Parser ();
				parser.load_from_file (Environment.get_home_dir () + "/.config/i3-scripts/.startup");
				return parser.get_root ().get_object ();
			} catch (Error e) {
				stderr.printf ("Caught an error: %s\n", e.message);
				Process.exit (1);
			}
		}

		public static Program load_program (string program_name) {
			var program = root.get_member (program_name).get_object ();
			var process_name = get_prop_or_default (program, "process_name", program_name);
			var command = get_prop_or_default (program, "command", process_name);
			var startup_dir = get_prop_or_default (program, "startup_dir", "/");
			var active = get_prop_or_default(program, "active", "yes");
			return new Program () {
				name = process_name,
				process_name = process_name,
				command = command,
				startup_dir = startup_dir,
				active = active == "yes"
			};
		}

		public static string get_prop_or_default (Json.Object program,
												  string prop, string def_value) {
			return program.has_member (prop) ?
				program.get_member (prop).get_string () :
				def_value;
		}
	}
}
