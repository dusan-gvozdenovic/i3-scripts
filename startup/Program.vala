using Startup.Utils;

namespace Startup {
	enum StartupStatus { SUCCESS, FAILURE, NOT_STARTED }

	class Program {
		public string name { get; set; }
		public string process_name { get; set; }
		public string command { get; set; }
		public string startup_dir { get; set; }
		public StartupStatus status = StartupStatus.NOT_STARTED;
		public string error_message { get; protected set; }
		public bool active { get; set; }

		public void start () {
			if (is_running () || !active) return;
			try {
				//Pid pid;
				Process.spawn_async (
					startup_dir,
					command.split_set (" \t"),
					Environ.get (),
					SpawnFlags.SEARCH_PATH | SpawnFlags.DO_NOT_REAP_CHILD,
					null,
					null);
				status = StartupStatus.SUCCESS;
			} catch (SpawnError e) {
				status = StartupStatus.FAILURE;
				error_message = @"Failed to start a program $name! Error: $(e.message)";
			}
		}

		public bool is_running () {
			try {
				string output;
				Process.spawn_command_line_sync (@"pidof $process_name", out output);
				return output != "";
			} catch (SpawnError e) {
				stderr.printf ("Failed to check if program is running. Startup process will now stop.\n");
				Process.exit (1);
			}
		}
	}
}
